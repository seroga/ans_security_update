#!/usr/bin/env python
# -*- coding: utf-8 -*-
import re


def remove_keys_from_dict(_dict, re_keys_list):
    """ regexp dict filter, 
    removes the keys matching with the regexp from re_keys_list """

    re_keys_list = [re.compile(item) for item in re_keys_list]
    filter_keys = _dict.keys()
    for k in filter_keys:
        for re_key in re_keys_list:
            if re_key.search(k):
                _dict.pop(k)
    return _dict


def leave_keys_in_dict(_dict, re_keys_list):
    """ regexp dict filter, 
    leave the keys matching with the regexp from re_keys_list """

    re_keys_list = [re.compile(item) for item in re_keys_list]
    new_dict = {}
    for k,v in _dict.items():
        for re_key in re_keys_list:
            if re_key.search(k):
                new_dict[k] = v
    return new_dict


class FilterModule(object):
    """ Ansible custom filter plugin """

    def filters(self):
        return {
            'remove_keys_from_dict': remove_keys_from_dict,
            'leave_keys_in_dict': leave_keys_in_dict
        }
