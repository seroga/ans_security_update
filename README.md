Intro
=====
1. Для работы необходим плагин filter_plugins/parse_dicts.py,
   [как подключить плагин можно узнать тут](http://docs.ansible.com/ansible/latest/intro_configuration.html#filter-plugins)

2. проверка доступных обновлений осуществляется через факт security_updates
3. Переменные:
   - update_cache_enable ('yes'|'no') включает или выключает кэш
   - exclude_package_patterns - фильтр который убирает паттерны
   - include_package_patterns - фильтр который оставляет только паттерны
4. Задать одновременно только один фильтр exclude_package_patterns или include_package_patterns

Examples
========
1. ansible-playbook security_updates.yaml --tags list_updates,needs_restart -i test.ini  -e hosts=web -e update_cache_enable=yes     
2. ansible-playbook security_updates.yaml --tags list_updates,needs_restart,upgrade_packages  -i test.ini    
3. ansible-playbook security_updates.yaml --tags list_updates,needs_restart,upgrade_packages_apt_get  -i test.ini     
4. ansible-playbook security_updates.yaml --tags list_updates,needs_restart,upgrade_packages  -i test.ini -e hosts=web     

